
package mvccalculadora;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

public class Controlador implements ActionListener
{
    
    private Modelo calc;
    private Vista vent;

    public Controlador(Modelo calc, Vista vent) {
        this.calc = calc;
        this.vent = vent;
    }
    
    
    public void iniciar()
        {
            vent.setTitle("Calculadora");
            vent.setLocationRelativeTo(null);
            vent.setVisible(true);
            
            asignarControl();
        }
    
     private void asignarControl()
        {
            vent.getCe().addActionListener(this);
            vent.getSumar().addActionListener(this);
            vent.getRestar().addActionListener(this);
            vent.getMultiplicar().addActionListener(this);
            vent.getDividir().addActionListener(this);
        }
     private void cerrar()
        {
            System.exit(0);
        }
     private void Ce ()
     {
         vent.getCuadro().setText("0");   
         vent.getCuadro2().setText("0");   
             vent.getRes().setText("");   
     }
     
     private void sumar()
     {
         float num1 = Float.parseFloat(vent.getCuadro().getText());
         float num2 = Float.parseFloat(vent.getCuadro2().getText());
         
         calc.setN1(num1);
         calc.setN2(num2);
         
         vent.getRes().setText(""+calc.Sumar());
     }
         
     private void restar()
     {
         float num1 = Float.parseFloat(vent.getCuadro().getText());
         float num2 = Float.parseFloat(vent.getCuadro2().getText());
         
         calc.setN1(num1);
         calc.setN2(num2);
         
         vent.getRes().setText(""+calc.Restar());
     }
     
     private void mult()
     {
         float num1 = Float.parseFloat(vent.getCuadro().getText());
         float num2 = Float.parseFloat(vent.getCuadro2().getText());
         
         calc.setN1(num1);
         calc.setN2(num2);
         
         vent.getRes().setText(""+calc.Multiplicar());
     }
     
     private void div()
     {
         float num1 = Float.parseFloat(vent.getCuadro().getText());
         float num2 = Float.parseFloat(vent.getCuadro2().getText());
         
         calc.setN1(num1);
         calc.setN2(num2);
         
         vent.getRes().setText(""+calc.Dividir());
     }
     

    @Override
    public void actionPerformed(ActionEvent e) 
    {
 
        if(e.getSource()== vent.getCe())
        {
            Ce();
        }
        if(e.getSource()== vent.getSumar())
        {
            sumar();
        }
        if(e.getSource()== vent.getRestar())
        {
            restar();
        }
        if(e.getSource()== vent.getMultiplicar())
        {
            mult();
        }
        if(e.getSource()== vent.getDividir())
        {
            div();
        }
        
    }
    
}
