
package mvccalculadora;

import java.awt.EventQueue;

public class MVCcalculadora 
{

    public static void main(String[] args) 
    {
       EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new Controlador(new Modelo(), new Vista()).iniciar();
            }
        }
        );
    }
    
}
